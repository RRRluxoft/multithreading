package multithread;

public class App {
    public static void main(String[] args) {
        Thread myThread = new MyThread();
        myThread.start();
        new MyThread().start();

//        Runnable myMainRunnable = new MyRunnable();
//        myMainRunnable.run();

        MyRunnable myRunnable = new MyRunnable();
        Thread threadFromRunnable = new Thread(myRunnable);
        threadFromRunnable.start();
    }
}

class MyThread extends Thread {
    @Override
    public void run() {
        System.out.println("this is new tread");
        for (int i = 0; i < 40; i++) {
        System.out.println("ThreadName is " + Thread.currentThread().getName() + " i =\t" + i);

        }
    }
}

class MyRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println("\t" + Thread.currentThread().getName());
        System.out.println("This is a new runnable");
    }
}
